import { test, expect } from '@playwright/test';
const testData = [{"ref":{"@ref":{"id":"315538282433741392","collection":{"@ref":{"id":"reminders","collection":{"@ref":{"id":"collections"}}}}}},"ts":1645910591470000,"data":{"user":1,"contents":"Star Trek discovery s4 e6"}},{"ref":{"@ref":{"id":"321833723614986833","collection":{"@ref":{"id":"reminders","collection":{"@ref":{"id":"collections"}}}}}},"ts":1644034056910000,"data":{"user":1,"contents":"Dentist"}}]

test.beforeEach(async ({ page }) => {
  await page.goto('http://localhost:3000/reminders');
  await page.route('https://rtb-netlify-functions.netlify.app/.netlify/functions/getReminders', async route => {
    await route.fulfill({
      contentType: "application/json",
      headers: { "access-control-allow-origin": "*" },
      status: 200,
      body: JSON.stringify(testData),
    });
  });
});


test.describe('Reminders', () => {
  test('add new reminder', async ({ page }) => {
    // Click [placeholder="Enter\ reminder\ here"]
    // await page.waitForResponse('https://rtb-netlify-functions.netlify.app/.netlify/functions/getReminders')
    await page.locator('[placeholder="Enter\\ reminder\\ here"]').click();
    // Fill [placeholder="Enter\ reminder\ here"]
    await page.locator('[placeholder="Enter\\ reminder\\ here"]').fill('This is a test reminder');
    // Click text=Done
    const request = page.waitForRequest('https://rtb-netlify-functions.netlify.app/.netlify/functions/addReminder')
    await page.locator('text=Done').click();

    await expect((await request).postData()).toEqual('{"userId":null,"contents":"This is a test reminder"}')
  });
})