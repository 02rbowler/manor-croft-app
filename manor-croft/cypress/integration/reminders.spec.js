describe('test reminders page', () => {
    it('should type in the input field', () => {
        cy.visit('/reminders');
        cy.get('input')
            .type('a test reminder')
            .should('have.value', 'a test reminder');
    })
})