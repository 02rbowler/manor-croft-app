describe('Navigation tests', () => {
    it('visits the homepage', () => {
        cy.visit('/');
        cy.contains('Reminders');
    });

    it('navigates to reminders', () => {
        cy.visit('/');
        cy.contains('Reminders').click();
        cy.url().should('include', '/reminders');
    });

    it('navigate to cctv', () => {
        cy.visit('/');
        cy.contains('CCTV').click();
        cy.url().should('include', '/cctv');
    });

    it('navigate to settings', () => {
        cy.visit('/');
        cy.contains('Settings').click();
        cy.url().should('include', '/settings');
    });

    it('navigate to homepage', () => {
        cy.visit('/settings');
        cy.contains('Dashboard').click();
        cy.contains("If the last update time");
    });
});