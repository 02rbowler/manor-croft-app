import { Channel } from "pusher-js";
import { AccountType, Config } from "./types";

export const pushToDashboard = (widget: string, accountValue: AccountType, channel?: Channel) => {
  channel && channel.trigger("client-" + accountValue, { widget });
}

export const pushCommandToDashboard = (command: string, accountValue: AccountType, channel?: Channel) => {
  channel && channel.trigger("client-" + accountValue, { command });
}

export const fetchReminders = async (account: AccountType, channel: Channel | undefined, accountNumber: number | null): Promise<any[]> => {
  if(channel) {
    channel.trigger("client-" + account, { command: "fetchReminders" });
  }
  return fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + '.netlify/functions/getReminders', {
    method: 'POST',
    body: JSON.stringify({userId: accountNumber})
  })
  .then(res => res.json())
  .then(res => {
    // setListData(res);
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}

export const fetchExerciseData = async (account: AccountType, channel: Channel | undefined, accountNumber: number | null): Promise<any[]> => {
  if(channel) {
    channel.trigger("client-" + account, { command: "fetchExercise" });
  }
  return fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + '.netlify/functions/getExerciseData', {
    method: 'POST',
    body: JSON.stringify({userId: accountNumber})
  })
  .then(res => res.json())
  .then(res => {
    // setListData(res);
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}

export const updateExerciseData = async ({accountNumber, text, ref}: {accountNumber: number | null, text: number[], ref: string}) => {
  return fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + '.netlify/functions/updateExerciseData', {
    method: 'POST',
    body: JSON.stringify({userId: accountNumber, ref, exercise: text})
  })
  // .then(res => res.json())
  .then(res => {
    // fetchReminders(account, channel, accountNumber);
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}


export const removeReminder = async ({id}: {id: number}) => {
  return await fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + '.netlify/functions/removeReminder', {
    method: 'POST',
    body: JSON.stringify({id})
  })
  .then(res => {
    // fetchReminders(account, channel, accountNumber);
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}

export const editReminder = async({id, accountNumber, editText}: {id: number, accountNumber: number | null, editText: string}) => {
  return fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + '.netlify/functions/updateReminder', {
    method: 'POST',
    body: JSON.stringify({
      ref: id,
      userId: accountNumber,
      contents: editText
    })
  })
  .then(res => {
    // fetchReminders(account, channel, accountNumber);
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}

export const addReminder = async ({accountNumber, text}: {accountNumber: number | null, text: string}) => {
  return fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + '.netlify/functions/addReminder', {
    method: 'POST',
    body: JSON.stringify({userId: accountNumber, contents: text})
  })
  // .then(res => res.json())
  .then(res => {
    // fetchReminders(account, channel, accountNumber);
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}

export const fetchWatchlist = async (account: AccountType, channel: Channel | undefined, accountNumber: number | null): Promise<any[]> => {
  // if(channel) {
  //   channel.trigger("client-" + account, { command: "fetchReminders" });
  // }
  return fetch(process.env.REACT_APP_OKTETO + '/getMyWatchlist?userId=' + accountNumber)
  .then(res => res.json())
  .then(res => {
    // setListData(res);
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}

export const addToWatchlist = async ({
  accountNumber, 
  id, 
  type, 
  name, 
  episode,
  poster_path,
  backdrop_path,
  overview
}: {
  accountNumber: number | null, 
  id: number,
  type: string,
  name: string,
  episode: string,
  poster_path: string,
  backdrop_path: string,
  overview: string
}) => {
  return fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + '.netlify/functions/addToWatchlist', {
    method: 'POST',
    body: JSON.stringify({
      userId: accountNumber,
      id,
      type,
      name,
      episode,
      poster_path,
      backdrop_path,
      overview
    })
  })
  // .then(res => res.json())
  .then(res => {
    // fetchReminders(account, channel, accountNumber);
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}

export const removeFromWatchlist = async (id: number) => {
  return await fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + '.netlify/functions/removeFromWatchlist', {
    method: 'POST',
    body: JSON.stringify({id})
  })
  .then(res => {
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}

export const editWatchlist = async({
  accountNumber, 
  id, 
  tmdbId,
  type, 
  name, 
  episode,
  poster_path
}: {
  accountNumber: number | null, 
  id: number,
  tmdbId: string,
  type: string,
  name: string,
  episode: string,
  poster_path: string
}) => {
  return fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + '.netlify/functions/updateWatchlist', {
    method: 'POST',
    body: JSON.stringify({
      ref: id,
      userId: accountNumber,
      id: tmdbId,
      type,
      name,
      episode,
      poster_path
    })
  })
  .then(res => {
    return res
  })
  .catch(err => {
    // Sentry.captureException("Reminders fetch failed: " + err);
  });
}

export const getDashboardConfig = async (): Promise<Config[]> => {
  return fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + ".netlify/functions/getDashboardConfig")
    .then(response => response.json())
    .then(response => {
      return response
    })
    .catch(error => {
      // callback(error);
    })
}
