export type widgetSpecCalendar = {
  tag: "calendar",
  widget: "calendar",
  displayName: string
  extra: {
    type: string
  }
}

export type widgetSpecWeather = {
  tag: "weather",
  widget: "weather",
  displayName: string
  extra: {
    location: string,
    type: string
  }
}

export type widgetSpecTV = {
  tag: "tv",
  widget: "tv",
  displayName: string
  extra: {
    type: string
  }
}

export type widgetSpecDefault = {
  tag: "default"
  widget: string
  displayName: string
}

export type ScheduleWidget = widgetSpecDefault | widgetSpecCalendar | widgetSpecWeather | widgetSpecTV

export interface Schedule {
  morning: ScheduleWidget[],
  afternoon: ScheduleWidget[],
  evening: ScheduleWidget[]
}

export interface FullSchedule {
  weekday: Schedule,
  weekend: Schedule
}

export interface ConfigData {
  displayName: string
  location: string
  email: string
  schedule: FullSchedule
  userId: number
}

export interface Config {
  data: ConfigData
  ref: {
    "@ref": {
      id: number
    }
  }
}

export type AccountType = string | null;