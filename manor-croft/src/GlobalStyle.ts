export const breakpoints = {
  widescreen: "@media screen and (min-width: 768px)"
}

export const colours = {
  darkBlue: "#211f3f"
}