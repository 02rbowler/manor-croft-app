import React, { useEffect, useState } from 'react';
// import styled from 'styled-components';
import Modal from '../components/Modal';
import ShadowedButton from '../components/ShadowedButton';
import Table from '../components/Table';

interface healthCheckItem {
  location: string,
  lastUpdate: number,
  version: string
}

function DashboardData() {
  const [healthCheckData, setHealthCheckData] = useState<healthCheckItem[]>([]);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const requestOptions = {
      method: 'POST'
    };

    function fetchData() {
      fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + ".netlify/functions/getMonitoring", requestOptions)
      .then(response => response.json())
      .then(response => {
        // callback(response);
        setHealthCheckData(response);
      })
      .catch(error => {
        // callback(error);
      })
    }

    let interval: NodeJS.Timeout;

    if(showModal) {
      fetchData();

      interval = setInterval(() => {
        fetchData();
      }, 300000);
    }

    return function cleanup() {
      clearInterval(interval);
    }
  }, [showModal])

  if(!showModal) {
    return (
      <ShadowedButton clickHandler={() => setShowModal(true)}>Dashboard data</ShadowedButton>
    )
  }

  return (
    <Modal title="Dashboard Data" closeModalFunc={() => setShowModal(false)}>
      <div style={{margin: "0 20px", marginTop: "20vh", textAlign: "center"}}>
        <h4>If the last update time is more than 15 minutes ago then the dashboard has stopped working</h4>
        <Table>
          <tr>
            <th>Location</th>
            <th>Last Update</th>
            <th>Version</th>
          </tr>
          {healthCheckData.map(item => (
            <tr key={item.location}>
              <td>{item.location}</td>
              <td>{(new Date(item.lastUpdate)).toLocaleTimeString()}</td>
              <td>{item.version}</td>
            </tr>
          ))}
        </Table>
      </div>
    </Modal>
  );
}

export default DashboardData;
