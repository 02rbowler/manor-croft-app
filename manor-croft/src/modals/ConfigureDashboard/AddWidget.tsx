import React, { useState } from 'react';
import styled from 'styled-components';
import { Block, BlockRow } from './style';
import { BsChevronDown } from 'react-icons/bs';
import { ScheduleWidget } from '../../types';

const TopRow = styled.div`
  margin: 30px 20px;
  font-weight: bold;

  span {
    width: 50%;
    display: inline-block;

    &:last-of-type {
      text-align: right;
    }
  }
`

const TextButton = styled.button`
  background: none;
  border: 0;
  color: inherit;
  font-size: inherit;
`

const Button = styled.span`
    font-weight: bold;
    cursor: pointer;
    display: flex;
    flex-direction: column;
    justify-content: center;
    color: white;
    background: linear-gradient(24deg,#ff1f91,#9200ce);
    margin-bottom: 10px;
    border-radius: 6px;
    padding: 0 10px;
    border: 1px solid #ff5dfa;
`;

const widgetOptions = {
  weather: {
    displayName: "Weather",
    requiredDetails: {
      location: "input"
    },
    options: [
      {
        type: "standard",
        displayText: "Standard"
      },
      {
        type: "forecast",
        displayText: "Forecast"
      }
    ]
  },
  sportsCentre: {
    displayName: "Sports Centre"
  },
  news: {
    displayName: "News"
  },
  birthdays: {
    displayName: "Upcoming birthdays"
  },
  tube: {
    displayName: "Tube"
  },
  tv: {
    displayName: "TV",
    options: [
      {
        type: "evening",
        displayText: "Evening schedule"
      }
    ]
  },
  podcast: {
    displayName: "Podcasts"
  }
}

const AddWidget: React.FC<{
  closeView: Function,
  addWidget: (newWidget: ScheduleWidget) => void
}> = ({closeView, addWidget}) => {
  const [weatherlocation, setWeatherLocation] = useState("");

  const renderExtras = (widgetKey: keyof typeof widgetOptions) => {
    switch(widgetKey) {
      case "weather": return <>
          <input type="text" value={weatherlocation} onChange={e => setWeatherLocation(e.target.value)} placeholder="Location" />
          {widgetOptions[widgetKey].options.map((option, idx) => (<BlockRow key={idx}>
            <span>{option.displayText}</span>
            <span>
              <Button onClick={() => weatherlocation.length > 0 && addWidget({
                tag: "weather",
                widget: widgetKey,
                displayName: widgetOptions[widgetKey].displayName,
                extra: {
                  location: weatherlocation.toLowerCase(),
                  type: option.type
                }
              })}>Add</Button>
            </span>
          </BlockRow>))}
        </>
      case "tv": return widgetOptions[widgetKey].options.map((option, idx) => (<BlockRow key={idx}>
          <span>{option.displayText}</span>
          <span>
            <Button onClick={() => addWidget({
              tag: "tv",
              widget: widgetKey,
              displayName: widgetOptions[widgetKey].displayName,
              extra: {
                type: option.type
              }
            })}>Add</Button>
          </span>
        </BlockRow>))
    }
  }

  return (
    <>
      <TopRow>
        <span>Add Widget</span>
        <span><TextButton onClick={() => closeView()}>Back</TextButton></span>
      </TopRow>
      {(Object.keys(widgetOptions) as Array<keyof typeof widgetOptions>).map((widgetKey, idx) => (
        <Block key={idx} style={{flexWrap: "wrap"}}>
          <BlockRow>
            <span>{widgetOptions[widgetKey].displayName}</span>
            <span>
              {'options' in widgetOptions[widgetKey] ? 
                <BsChevronDown /> :
                <Button onClick={() => addWidget({
                  tag: "default",
                  widget: widgetKey,
                  displayName: widgetOptions[widgetKey].displayName
                })}>Add</Button>
              }
            </span>
          </BlockRow>
          {'options' in widgetOptions[widgetKey] && 
            <div style={{width: '100%', marginTop: '10px'}}>
              { renderExtras(widgetKey) }
            </div>
          }
        </Block>
      ))}
    </>
  );
}

export default AddWidget;
