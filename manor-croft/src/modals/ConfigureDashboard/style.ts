import styled from "styled-components"

export const Block = styled.div`
  margin: 10px 20px;
  background-color: #3a3382;
  min-height: 80px;
  display: flex;
  align-items: center;
  border-radius: 10px;
  width: 18px;
  font-weight: bold;
  padding: 5px 15px;
  width: calc(100% - 40px);
  box-sizing: border-box;
`

export const BlockRow = styled.div`
  display: flex;
  width: 100%;

  span:first-of-type {
    flex: 1;
  }
`