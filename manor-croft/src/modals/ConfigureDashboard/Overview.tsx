import React from 'react';
import Row from '../../components/Row';
import { FullSchedule, ScheduleWidget } from '../../types';
import { Block, BlockRow } from './style';

const BlockRowElem: React.FC<{
  blockText: string, 
  schedule: ScheduleWidget[],
  clickHandler: (displayName: string, schedule: ScheduleWidget[]) => void
}> = ({blockText, schedule, clickHandler}) => (
  <Block onClick={() => clickHandler(blockText, schedule)}>
    <BlockRow>
      <span>{blockText}</span>
      <span>{schedule.length}</span>
    </BlockRow>
  </Block>
)

const Overview: React.FC<{
  displayName: string, 
  schedule: FullSchedule, 
  clickHandler: (displayName: string, schedule: ScheduleWidget[]) => void
}> = ({displayName, schedule, clickHandler}) => {
  return (
    <>
      <Row><h3>Dashboard: {displayName}</h3></Row>
      <BlockRowElem 
        blockText="Weekday - morning"
        schedule={schedule.weekday.morning}
        clickHandler={clickHandler}
      />
      <BlockRowElem 
        blockText="Weekday - afternoon"
        schedule={schedule.weekday.afternoon}
        clickHandler={clickHandler}
      />
      <BlockRowElem 
        blockText="Weekday - evening"
        schedule={schedule.weekday.evening}
        clickHandler={clickHandler}
      />
      <BlockRowElem 
        blockText="Weekend - morning"
        schedule={schedule.weekend.morning}
        clickHandler={clickHandler}
      />
      <BlockRowElem 
        blockText="Weekend - afternoon"
        schedule={schedule.weekend.afternoon}
        clickHandler={clickHandler}
      />
      <BlockRowElem 
        blockText="Weekend - evening"
        schedule={schedule.weekend.evening}
        clickHandler={clickHandler}
      />
    </>
  );
}

export default Overview;
