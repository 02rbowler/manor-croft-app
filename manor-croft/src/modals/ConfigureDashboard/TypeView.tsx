import React from 'react';
import { AiOutlineArrowDown, AiOutlineArrowUp, AiOutlineClose, AiOutlinePlus } from 'react-icons/ai';
import styled from 'styled-components';
import { ScheduleWidget, widgetSpecCalendar, widgetSpecTV, widgetSpecWeather } from '../../types';
import { Block, BlockRow } from './style';

const TopRow = styled.div`
  margin: 30px 20px;
  font-weight: bold;

  span {
    width: 33%;
    display: inline-block;

    &:last-of-type {
      text-align: right;
    }
  }
`

const TextButton = styled.button`
  background: none;
  border: 0;
  color: inherit;
  font-size: inherit;
`

const SmallDiv = styled.div`
  font-size: 12px;
`

const ButtonContainer = styled.span`
  display: flex;
  align-items: center;

  span:first-of-type {
    margin-right: 10px;
  }
`

const TypeView: React.FC<{
  type: string, 
  widgets: ScheduleWidget[], 
  closeView: Function,
  deleteRow: Function,
  reOrderRow: Function,
  addNew: Function
}> = ({type, widgets, closeView, deleteRow, reOrderRow, addNew}) => {
  return (
    <>
      <TopRow>
        <span><TextButton onClick={() => closeView()}>Save/Back</TextButton></span>
        <span>{type}</span>
        <span><AiOutlinePlus onClick={() => addNew()} /></span>
      </TopRow>
      {widgets.map((widget, idx) => (
        <Block key={idx}>
          <BlockRow>
            <span>{
              widget.widget === "calendar" ? <>
                <div>{widget.displayName}</div>
                <SmallDiv>{(widget as widgetSpecCalendar).extra.type}</SmallDiv>
              </> :
              widget.widget === "weather" ? <>
                <div>{widget.displayName}</div>
                <SmallDiv>
                  {(widget as widgetSpecWeather).extra.location},&nbsp;
                  {(widget as widgetSpecWeather).extra.type}
                </SmallDiv>
              </> :
              widget.widget === "tv" ? <>
                <div>{widget.displayName}</div>
                <SmallDiv>
                  {(widget as widgetSpecTV).extra.type}
                </SmallDiv>
              </> :
              widget.displayName
            }</span>
            <ButtonContainer>
              <span>
                {idx !== 0 && <div onClick={() => reOrderRow(idx, "up")}><AiOutlineArrowUp size={20} /></div>}
                {idx + 1 < widgets.length && <div onClick={() => reOrderRow(idx, "down")}><AiOutlineArrowDown size={20} /></div>}
              </span>
              {widgets.length > 1 && <span onClick={() => deleteRow(idx)}>
                <AiOutlineClose size={20} />
              </span>}
            </ButtonContainer>
          </BlockRow>
        </Block>
      ))}
    </>
  );
}

export default TypeView;
