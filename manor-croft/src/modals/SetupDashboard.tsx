import React, { FC, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import Modal from '../components/Modal';
import Row from '../components/Row';
import RowButton from '../components/RowButton';

const Input = styled.input`
  font-size: 60px;
  font-weight: bold;
  text-align: center;
  width: calc(100% - 40px);
  margin: 0 20px;
  border-radius: 10px;
  border: 0;
  box-sizing: border-box;
  letter-spacing: 10px;
  background-color: #e2e2e2;
`

const SelectElem = styled.select`
  display: block;
  width: 100%;
  font-size: 30px;
  border: 0;
  border-radius: 5px;
  padding: 5px 10px;
  margin-bottom: 20px;
`

const ButtonText = styled.button`
  border: 0;
  background: none;
  color: #0372ff;
  font-size: 20px;
  font-weight: bold;
  padding: 10px;
`

const SetupDashboard: FC<{locations: {
  location: string;
  displayName: string;
}[]}> = ({locations}) => {
    const [code, setCode] = useState('');
    const [location, setLocation] = useState('');
    const [showModal, setShowModal] = useState(false);
    const [screen, setScreen] = useState(1);
    const [sendingRequest, setSendingRequest] = useState(false);
    const inputEl = useRef<HTMLInputElement>(null);

    const sendRequest = () => {
      if(code.length === 5 && location !== '') {
        setSendingRequest(true);

        fetch(process.env.REACT_APP_ENDPOINT + 'pusher/setupdashboard', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({code: code, location: location})
        })
        .then(response => {setShowModal(false)})
        .catch(err => {
          // Sentry.captureException("Reminders fetch failed: " + err);
          setSendingRequest(false);
        });
      }
    }

    useEffect(() => {
      setScreen(1);
      setLocation('');
      setCode('');
      setSendingRequest(false);
    }, [showModal])

    if(!showModal) {
      return (
        <Row>
          <div onClick={() => setShowModal(true)}>
            <RowButton text="+ Setup dashboard" />
          </div>
        </Row>
      )
    }

    return (
      <Modal title="Setup Dashboard" closeModalFunc={() => setShowModal(false)}>
        <div style={{margin: "0 20px", marginTop: "20vh", textAlign: "center"}}>
          {screen === 1 && <>
            <Input ref={inputEl} type="number" />
            <h4>Enter the code displayed on the dashboard screen then click next</h4>
            <ButtonText onClick={() => {inputEl.current && setCode(inputEl.current.value); setScreen(2)}}>Next</ButtonText>
          </>}
          {screen === 2 && <>
            <h4>Select the profile of the dashboard</h4>
            <SelectElem value={location} onChange={(e) => setLocation(e.target.value)}>
              <option value=""></option>
              {locations.map(loc => 
                <option key={loc.location} value={loc.location}>{loc.displayName}</option>  
              )}
            </SelectElem>
            {!sendingRequest ?
              <ButtonText onClick={() => sendRequest()}>Confirm</ButtonText>
              : <h5>Allow up to 10 seconds for the dashboard to connect</h5>
            }
          </>}
        </div>
      </Modal>
    );
}

export default SetupDashboard;
