import React, { useContext, useEffect, useState } from 'react';
import Modal from '../components/Modal';
import ShadowedButton from '../components/ShadowedButton';
import { ConfigContext } from '../App';
import { AccountType, FullSchedule, ScheduleWidget } from '../types';
import Overview from './ConfigureDashboard/Overview';
import TypeView from './ConfigureDashboard/TypeView';
import AddWidget from './ConfigureDashboard/AddWidget';

type Displays = "overview" | "typeView" | "addWidget";

const ConfigureDashboard:React.FC<{account: AccountType}> = ({account}) => {
  const [showModal, setShowModal] = useState(false);
  const [displayMode, setDisplayMode] = useState<Displays>("overview");
  const [displayedSchedule, setDisplayedSchedule] = useState<ScheduleWidget[] | null>(null);
  const [displayedType, setDisplayedType] = useState("");
  const config = useContext(ConfigContext);
  const [schedule, setSchedule] = useState(config?.data.schedule)

  const displayType = (displayName: string, schedule: ScheduleWidget[]) => {
    setDisplayedType(displayName);
    setDisplayedSchedule(schedule);
    setDisplayMode("typeView");
  }
  
  useEffect(() => {
    setSchedule(config?.data.schedule);
  }, [config]);

  if(!showModal) {
    return (
      <ShadowedButton clickHandler={() => setShowModal(true)}>Configure dashboard</ShadowedButton>
    )
  }

  const deleteRow = (deletedIndex: number) => {
    let newSchedule = schedule as FullSchedule;

    if(displayedType.indexOf("Weekend") === 0) {
      if(displayedType.indexOf("morning") !== -1) {
        newSchedule.weekend.morning = newSchedule.weekend.morning.filter((item, idx) => idx !== deletedIndex)
      } else if(displayedType.indexOf("afternoon") !== -1) {
        newSchedule.weekend.afternoon = newSchedule.weekend.afternoon.filter((item, idx) => idx !== deletedIndex)
      } else if(displayedType.indexOf("evening") !== -1) {
        newSchedule.weekend.evening = newSchedule.weekend.evening.filter((item, idx) => idx !== deletedIndex)
      }
    } else if(displayedType.indexOf("Weekday") === 0) {
      if(displayedType.indexOf("morning") !== -1) {
        newSchedule.weekday.morning = newSchedule.weekday.morning.filter((item, idx) => idx !== deletedIndex)
      } else if(displayedType.indexOf("afternoon") !== -1) {
        newSchedule.weekday.afternoon = newSchedule.weekday.afternoon.filter((item, idx) => idx !== deletedIndex)
      } else if(displayedType.indexOf("evening") !== -1) {
        newSchedule.weekday.evening = newSchedule.weekday.evening.filter((item, idx) => idx !== deletedIndex)
      }
    }

    setSchedule(newSchedule);
    setDisplayedSchedule((displayedSchedule as ScheduleWidget[]).filter((item, idx) => idx !== deletedIndex));
  }

  const editSchedule = (editingSchedule: ScheduleWidget[], direction: "up" | "down", rowIndex: number) => {
    let changingValue = editingSchedule[rowIndex];
    let otherValue = editingSchedule[direction === "up" ? rowIndex - 1 : rowIndex + 1];
    editingSchedule[rowIndex] = otherValue;
    editingSchedule[direction === "up" ? rowIndex - 1 : rowIndex + 1] = changingValue;
    return editingSchedule
  }

  const reOrderRow = (rowIndex: number, direction: "up" | "down") => {
    let newSchedule = schedule as FullSchedule;

    if(displayedType.indexOf("Weekend") === 0) {
      if(displayedType.indexOf("morning") !== -1) {
        newSchedule.weekend.morning = editSchedule(newSchedule.weekend.morning, direction, rowIndex);
        setDisplayedSchedule([...newSchedule.weekend.morning]);
      } else if(displayedType.indexOf("afternoon") !== -1) {
        newSchedule.weekend.afternoon = editSchedule(newSchedule.weekend.afternoon, direction, rowIndex);
        setDisplayedSchedule([...newSchedule.weekend.afternoon]);
      } else if(displayedType.indexOf("evening") !== -1) {
        newSchedule.weekend.evening = editSchedule(newSchedule.weekend.evening, direction, rowIndex);
        setDisplayedSchedule([...newSchedule.weekend.evening]);
      }
    } else if(displayedType.indexOf("Weekday") === 0) {
      if(displayedType.indexOf("morning") !== -1) {
        newSchedule.weekday.morning = editSchedule(newSchedule.weekday.morning, direction, rowIndex);
        setDisplayedSchedule([...newSchedule.weekday.morning]);
      } else if(displayedType.indexOf("afternoon") !== -1) {
        newSchedule.weekday.afternoon = editSchedule(newSchedule.weekday.afternoon, direction, rowIndex);
        setDisplayedSchedule([...newSchedule.weekday.afternoon]);
      } else if(displayedType.indexOf("evening") !== -1) {
        newSchedule.weekday.evening = editSchedule(newSchedule.weekday.evening, direction, rowIndex);
        setDisplayedSchedule([...newSchedule.weekday.evening]);
      }
    }

    setSchedule(newSchedule);
  }

  const addWidget = (newWidget: ScheduleWidget) => {
    let newSchedule = schedule as FullSchedule;

    if(displayedType.indexOf("Weekend") === 0) {
      if(displayedType.indexOf("morning") !== -1) {
        newSchedule.weekend.morning.push(newWidget);
        setDisplayedSchedule([...newSchedule.weekend.morning]);
      } else if(displayedType.indexOf("afternoon") !== -1) {
        newSchedule.weekend.afternoon.push(newWidget);
        setDisplayedSchedule([...newSchedule.weekend.afternoon]);
      } else if(displayedType.indexOf("evening") !== -1) {
        newSchedule.weekend.evening.push(newWidget);
        setDisplayedSchedule([...newSchedule.weekend.evening]);
      }
    } else if(displayedType.indexOf("Weekday") === 0) {
      if(displayedType.indexOf("morning") !== -1) {
        newSchedule.weekday.morning.push(newWidget);
        setDisplayedSchedule([...newSchedule.weekday.morning]);
      } else if(displayedType.indexOf("afternoon") !== -1) {
        newSchedule.weekday.afternoon.push(newWidget);
        setDisplayedSchedule([...newSchedule.weekday.afternoon]);
      } else if(displayedType.indexOf("evening") !== -1) {
        newSchedule.weekday.evening.push(newWidget);
        setDisplayedSchedule([...newSchedule.weekday.evening]);
      }
    }

    setSchedule(newSchedule);
    setDisplayMode("typeView")
  }

  const saveChanges = () => {
    setDisplayMode("overview")
    fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + ".netlify/functions/updateDashboardConfig", {
      method: 'POST',
      body: JSON.stringify({ref: config?.ref['@ref'].id, data: config?.data})
    })
    .catch(err => {
      // Sentry.captureException("Reminders fetch failed: " + err);
    });
  }

  return (
    <Modal title="Configure Dashboard" closeModalFunc={() => {
      setDisplayMode("overview");
      setShowModal(false);
    }}>
      {
        schedule && displayMode === "overview" && 
          <Overview 
            displayName={config?.data.displayName || ""} 
            schedule={schedule}
            clickHandler={displayType}
          />
      }
      {
        displayMode === "typeView" && displayedSchedule &&
          <TypeView 
            type={displayedType} 
            widgets={displayedSchedule} 
            closeView={saveChanges} 
            deleteRow={deleteRow}
            reOrderRow={reOrderRow}
            addNew={() => setDisplayMode("addWidget")}
          />
      }
      {
        displayMode === "addWidget" && displayedSchedule &&
          <AddWidget
            closeView={() => setDisplayMode("typeView")}
            addWidget={addWidget}
          />
      }
    </Modal>
  );
}

export default ConfigureDashboard;
