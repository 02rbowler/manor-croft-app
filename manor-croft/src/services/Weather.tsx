import React from "react";
import { WiDaySunny, WiCloud, WiDaySunnyOvercast, WiCloudyWindy, WiSnow, WiRainMix, WiRain, WiThunderstorm } from "react-icons/wi";

const iconSize = 30;

export const getIcon = (weather: string): React.ReactNode => {
  let weatherIcon = <WiDaySunny size={iconSize} />;

  switch(weather.toLowerCase()){
      case 'clouds': weatherIcon = <WiCloud size={iconSize} />; break;
      case 'clear': weatherIcon = <WiDaySunny size={iconSize} />; break;
      case 'ash':
      case 'dust':
      case 'sand':
      case 'fog':
      case 'haze':
      case 'smoke':
      case 'mist':
          weatherIcon = <WiDaySunnyOvercast size={iconSize} />; break;
      case 'tornado':
      case 'squall':
          weatherIcon = <WiCloudyWindy size={iconSize} />; break;
      case 'snow': weatherIcon = <WiSnow size={iconSize} />; break;
      case 'rain': weatherIcon = <WiRain size={iconSize} />; break;
      case 'drizzle': weatherIcon = <WiRainMix size={iconSize} />; break;
      case 'thunderstorm': weatherIcon = <WiThunderstorm size={iconSize} />; break;
  }

  return weatherIcon;
};
