import React from 'react';
import Heading from '../components/Heading';
import styled from 'styled-components';

const VideoContainer = styled.div`
	max-width: 1000px;

	iframe {
		width: 100%;
		border: 0;
		aspect-ratio: auto 704 / 576
	}
`;

const Container = styled.div`
	margin: 20px;
`

function CCTV() {
    // const canvas = useRef<HTMLCanvasElement>(null);
		// const canvasFrontDoor = useRef<HTMLCanvasElement>(null);

    // useEffect(() => {
		// 	if (canvas.current) {
		// 		loadPlayer({
		// 			url: 'wss://192.168.5.66:3002/api/stream',
		// 			canvas: canvas.current,
		// 			audio: false,
		// 			disconnectThreshold: 5000
		// 		});
		// 	}

		// 	if(canvasFrontDoor.current) {
		// 		loadPlayer({
		// 			url: 'wss://192.168.5.66:3002/api/streamFrontDoor',
		// 			canvas: canvasFrontDoor.current,
		// 			audio: false,
		// 			disconnectThreshold: 5000
		// 		});
		// 	}
		// }, []);

    return (
			<Container>
				<Heading>CCTV</Heading>
				<VideoContainer>
					<iframe id="mainFeed" title="mainfeed" src="https://manorcroft.webredirect.org/cctvapi" />
					<iframe id="frontDoor" title="frontDoor" src="https://manorcroft.webredirect.org/cctvapi/frontdoor" />
					{/* <IframeResizer src="http://192.168.5.66:3002" heightCalculationMethod="lowestElement" style={{ width: '1px', minWidth: '100%'}} /> */}
					{/* <canvas ref={canvas} style={{width: "100%"}} />
					<canvas ref={canvasFrontDoor} style={{width: "100%", transform: "rotate(180deg)"}} /> */}
				</VideoContainer>
			</Container>
    );
}

export default CCTV;
