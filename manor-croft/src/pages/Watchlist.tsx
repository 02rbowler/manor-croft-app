import React, { useCallback, useContext, useEffect, useState } from 'react';
import Heading from '../components/Heading';
import { Channel } from 'pusher-js';
import Row from '../components/Row';
import styled from 'styled-components';
import { AccountContext } from '../App';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { addToWatchlist, fetchWatchlist, removeFromWatchlist } from '../api';
import debounce from 'lodash.debounce';

// const DeleteButton = styled.button`
//   color: white;
//   background-color: #d20000;
//   border: 1px solid #ff8383;
//   margin-left: auto;
//   padding: 5px;
//   border-radius: 5px;
//   justify-content: center;
//   align-items: center;
//   display: flex;
// `

// const EditButton = styled(DeleteButton)`
//   background-color: blue;
//   margin-right: 10px;
// `

// const GreenButton = styled(DeleteButton)`
//   background-color: green;
// `

const ImgRow = styled.div`
  margin-top: 10px;
  display: flex;

  img {
    width: 200px;
    margin-right: 10px;
    border-radius: 10px;
  }
`

const ListDetail = styled.div`
  background-color: white;
  color: black;
  text-align: center;
  margin-right: 10px;
`

export interface Suggestion {
  id: number
  poster_path: string
  name: string
  title: string
  backdrop_path: string
  overview: string
}

interface propTypes {
  channel?: Channel
  accountNumber: number | null
}

const Watchlist: React.FC<propTypes> = ({channel, accountNumber}) => {
    const {account} = useContext(AccountContext);
    const [inputTitle, setInputTitle] = useState("")
    const [suggestions, setSuggestions] = useState<Suggestion[]>([]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const debouncedFetch = useCallback(
      debounce(async (searchParam, type) => {
        if(searchParam) {
          const res = await fetch(`https://api.themoviedb.org/3/search/${type}?api_key=67982f3ab5504fdd803f32b9baf6ac43&query=${searchParam}`)
          const resJson = await res.json()
          if(resJson.results) {
            console.log(resJson.results)
            setSuggestions(resJson.results.slice(0, 5).filter((item: Suggestion) => item.poster_path));
          }
        }
      }, 400),
      [], // will be created only once initially
    );

    useEffect(() => {
      // if(inputTitle.length > 0 && (inputType === "Movie" || inputType === "TV Show")){
      //   debouncedFetch(inputTitle, inputType === "Movie" ? 'movie': 'tv');
      // }
      debouncedFetch(inputTitle, 'tv');
    }, [inputTitle, debouncedFetch]);

    const queryClient = useQueryClient()
 
    // Queries
    const query = useQuery('watchlist', () => fetchWatchlist(account, channel, accountNumber))

    // Mutations
    // const editMutation = useMutation(editWatchlist, {
    //   onSuccess: () => {
    //     // Invalidate and refetch
    //     queryClient.invalidateQueries('watchlist')
    //   },
    // })

    const addMutation = useMutation(addToWatchlist, {
      onSuccess: () => {
        // Invalidate and refetch
        queryClient.invalidateQueries('watchlist')
      },
    })

    const removeMutation = useMutation(removeFromWatchlist, {
      onSuccess: () => {
        // Invalidate and refetch
        queryClient.invalidateQueries('watchlist')
      },
    })

    const selectSuggestion = (suggestion: Suggestion) => {
      console.log(suggestion)
      addMutation.mutate({
        accountNumber,
        id: suggestion.id,
        type: "tv",
        episode: "1.0",
        name: suggestion.name,
        poster_path: suggestion.poster_path,
        backdrop_path: suggestion.backdrop_path,
        overview: suggestion.overview
      })
    }
    console.log(query)

    return (
      <>
        <Row>
          <Heading>Watchlist</Heading>
        </Row>
        <Row>
          <input value={inputTitle} onChange={e => setInputTitle(e.target.value)} />
          <ImgRow>
            {suggestions.map(suggestion => <img key={suggestion.id} onClick={() => selectSuggestion(suggestion)} src={`https://image.tmdb.org/t/p/w500${suggestion.poster_path}`} alt="" />)}
          </ImgRow>
          {/* <TextBox value={""} withButton={"Done"} clickHandler={(inputText: string) => {
            addMutation.mutate({
              accountNumber,
              text: inputText
            })
          }} placeholder="Enter show/film name" /> */}
          <div>My List</div>
          <ImgRow>
            {query.data?.map(listItem => <div key={listItem.id}>
              <img src={`https://image.tmdb.org/t/p/w500${listItem.poster_path}`} alt="" />
              {listItem.type === "tv" && listItem.nextEpisode && <ListDetail>{`S${listItem.nextEpisode.split('.')[0]} E${listItem.nextEpisode.split('.')[1]}`}</ListDetail>}
              <button onClick={() => removeMutation.mutate(listItem.ref)}>Remove</button>
            </div>)}
          </ImgRow>
        </Row>
        <br />
        
      </>
    );
}

export default Watchlist;
