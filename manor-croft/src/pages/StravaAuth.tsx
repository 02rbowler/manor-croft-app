import React, { useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';

const StravaAuth:React.FC = () => {
  console.log("MADE IT")
  const [searchParams,] = useSearchParams();
  const code = searchParams.get('code')

  useEffect(() => {
    // process.env.REACT_APP_NETLIFY_ENDPOINT + ".netlify/functions/updateDashboardConfig"
    fetch(`https://www.strava.com/oauth/token?client_id=59699&client_secret=d545a4a7212e2bf36f06babab6fd91d89e12469b&code=${code}&grant_type=authorization_code`, {
      method: 'POST',
      // body: JSON.stringify({ref: config?.ref['@ref'].id, data: config?.data})
    })
    .then(res => res.json())
    .then(res => {
      // const refresh = res['refresh_token'];
      // const access = res['access_token'];
      console.log(res)
    })
    .catch(err => {
      // Sentry.captureException("Reminders fetch failed: " + err);
    });
  }, [code])

  return null;
}

export default StravaAuth;
