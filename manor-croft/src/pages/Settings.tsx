import React, { useContext } from 'react';
import styled from 'styled-components';
import Heading from '../components/Heading';
import Row, { ColouredRow } from '../components/Row';
import RowColumn from '../components/RowColumn';
import SetupDashboard from '../modals/SetupDashboard';
import packageInfo from './../../package.json';
import {FaSpotify, FaStrava} from 'react-icons/fa'
import {MdAccountCircle} from 'react-icons/md'
import { AccountContext } from '../App';
import { AccountType } from '../types';
import { Link } from 'react-router-dom';
// import { useAuth0 } from '@auth0/auth0-react';
// import Loading from '../components/Loading';

const RowIcon = styled.div`
  color: white;
  background-color: #484848;
  border: 1px solid #696363;
  margin-right: 10px;
  padding: 5px;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
  display: flex;
`

const AnchorLink = styled.a`
  color: white;
  padding: 5px;
  background-color: #5a5a5a;
  margin-right: 5px;
  text-align: center;
  display: block;
  border-radius: 5px;
  text-decoration: none;
`

const Select = styled.select`
  width: calc(100% - 5px);
  margin-right: 5px;
  border: 0;
  border-radius: 5px;
  padding: 5px;
`

// const LoginButton = styled.button`
//   width: 100%;
//   font-weight: bold;
//   padding: 0 15px;
//   border-radius: 10px;
//   text-align: left;
//   background: linear-gradient(24deg,#12fffe,#a4da00);
//   color: black;
//   border: 1px solid #5bffff;
//   display: block;

//   &:hover {
//     cursor: pointer;
//   }
// `

const Settings:React.FC<{locations: {location: string, displayName: string}[]}> = ({locations}) => {
  const {account, updateAccount} = useContext(AccountContext);
  // const { loginWithRedirect, user, isAuthenticated, isLoading, getAccessTokenSilently } = useAuth0();

  // useEffect(() => {
  //   if(isAuthenticated) {
  //     getAccessTokenSilently()
  //       .then(ret => {
  //         console.log(ret)
  //       })
  //   }
  // }, [isAuthenticated])

  // console.log(user, isAuthenticated, isLoading);

  const updateAccountVal = (option: AccountType) => {
    localStorage.setItem('manor_croft_app_account', option || "");
    updateAccount(option);
  }
console.log('account', account)
  return (
    <>
      <Row>
        <Heading>Settings</Heading>
      </Row>
      {/* { isAuthenticated ?
        <> */}
          <ColouredRow>
            <RowIcon>
              <MdAccountCircle size={20} />
            </RowIcon>
            <RowColumn>Account</RowColumn>
            <RowColumn>
              <Select value={account as string} onChange={(e) => {updateAccountVal(e.target.value as AccountType)}}>
                <option value=""></option>
                {locations.map(val => (
                  <option key={val.displayName} value={val.location}>{val.displayName}</option>
                ))}
                <option value="test">Test</option>
              </Select>
            </RowColumn>
          </ColouredRow>
          {account === "rob_bedroom" && <>
            <ColouredRow>
              <RowIcon>
                <FaSpotify size={20} />
              </RowIcon>
              <RowColumn>Spotify</RowColumn>
              <RowColumn>
                <AnchorLink href={`${process.env.REACT_APP_ENDPOINT}spotify/authorise`}>Connect account</AnchorLink>
              </RowColumn>
            </ColouredRow>
            <ColouredRow>
              <RowIcon>
                <FaStrava size={20} />
              </RowIcon>
              <RowColumn>Strava</RowColumn>
              <RowColumn>
                <AnchorLink href={`http://www.strava.com/oauth/authorize?client_id=59699&response_type=code&redirect_uri=http://localhost:3000/stravaAuth&approval_prompt=force&scope=read`}>Connect account</AnchorLink>
              </RowColumn>
            </ColouredRow>
          </>}
          {account === 'test' && <Link to='/map' style={{color: 'red'}}>Go to map</Link>}

          <SetupDashboard locations={locations} />
        {/* </>
        : isLoading ? <Loading />
        : 
          <Row>
            <LoginButton onClick={() => loginWithRedirect()}><h3>Log In</h3></LoginButton>
          </Row>
      } */}

      <Row>
        Version: {packageInfo.version}
      </Row>
    </>
    );
}

export default Settings;
