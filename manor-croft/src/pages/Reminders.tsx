import React, { useContext, useState } from 'react';
import Heading from '../components/Heading';
import TextBox from '../components/TextBox';
import { Channel } from 'pusher-js';
import Row, { ColouredRow } from '../components/Row';
import styled from 'styled-components';
import {AiOutlineCheck, AiOutlineClose, AiOutlineEdit} from 'react-icons/ai';
import { AccountContext } from '../App';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { editReminder, fetchReminders, removeReminder, addReminder } from '../api';

const DeleteButton = styled.button`
  color: white;
  background-color: #d20000;
  border: 1px solid #ff8383;
  margin-left: auto;
  padding: 5px;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
  display: flex;
`

const EditButton = styled(DeleteButton)`
  background-color: blue;
  margin-right: 10px;
`

const GreenButton = styled(DeleteButton)`
  background-color: green;
`

interface propTypes {
  channel?: Channel
  accountNumber: number | null
}

const Reminders:React.FC<propTypes> = ({channel, accountNumber}) => {
    const [editIndex, setEditIndex] = useState(-1);
    const [editText, setEditText] = useState("");
    const {account} = useContext(AccountContext);

    const queryClient = useQueryClient()
 
    // Queries
    const query = useQuery('reminders', () => fetchReminders(account, channel, accountNumber))

    // Mutations
    const editMutation = useMutation(editReminder, {
      onSuccess: () => {
        // Invalidate and refetch
        queryClient.invalidateQueries('reminders')
      },
    })

    const addMutation = useMutation(addReminder, {
      onSuccess: () => {
        // Invalidate and refetch
        queryClient.invalidateQueries('reminders')
      },
    })

    const removeMutation = useMutation(removeReminder, {
      onSuccess: () => {
        // Invalidate and refetch
        queryClient.invalidateQueries('reminders')
      },
    })

    return (
      <>
        <Row>
          <Heading>Reminders</Heading>
        </Row>
        <Row>
          <TextBox value={""} withButton={"Done"} clickHandler={(inputText: string) => {
            addMutation.mutate({
              accountNumber,
              text: inputText
            })
          }} placeholder="Enter reminder here" />
        </Row>
        <br />
        {query && query.data && query.data.map((item: any, idx: number) => (
          <ColouredRow key={item.ref["@ref"].id}>
            <span style={{paddingLeft: "10px", flex: "1"}}>
              {editIndex === idx ?
                <input type="text" value={editText} onChange={(e) => setEditText(e.target.value)} />
              : item.data.contents}
            </span>
            <span style={{display: "flex"}}>
              {
                editIndex === idx ?
                  <GreenButton onClick={() => {
                    setEditIndex(-1);
                    editMutation.mutate({
                      id: item.ref["@ref"].id,
                      accountNumber,
                      editText
                    })
                  }}>
                    <AiOutlineCheck size={20} />
                  </GreenButton>
                : <>
                  <EditButton onClick={() => {
                    setEditIndex(idx)
                    setEditText(item.data.contents)
                  }}>
                    <AiOutlineEdit size={20} />
                  </EditButton>
                  <DeleteButton onClick={() => removeMutation.mutate({
                    id: item.ref["@ref"].id,
                  })}>
                    <AiOutlineClose size={20} />
                  </DeleteButton>
                </>
              }
            </span>
          </ColouredRow>
        ))}
      </>
    );
}

export default Reminders;
