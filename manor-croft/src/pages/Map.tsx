import React, { useContext, useEffect, useMemo, useState } from 'react';
import { Channel } from 'pusher-js';
import styled from 'styled-components';
import cover from '../assets/cover.jpg';
import { GoogleMap, Marker, useJsApiLoader } from '@react-google-maps/api';
import nameLabel from '../assets/namelabel.svg';
import faunadb from 'faunadb';
import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition';
import { MapCover } from '../components/MapCover';
import rob from '../RobImg.png'
import becca from '../BeccaImg.png'
import srija from '../SrijaImg.png'
import jasmin from '../JasminImg.png'
// import { useAuth0 } from '@auth0/auth0-react';

const CoverImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`

type propTypes = {
  channel?: Channel
}

const MessageOverlay = styled.div`
  position: absolute;
  z-index: 100;
  bottom: 20px;
  left: 20px;
  right: 20px;
  background-color: #5B0F11;
  border: 1px solid black;
  padding: 20px;
  border-radius: 10px;
  text-align: center;
`

const BackupImage = styled.img`
  object-fit: cover;
  width: 100%;
  height: 100%;
`

const exampleMapStyles = [
  {
      featureType: "poi",
      elementType: "geometry",
      stylers: [
          {
              color: "#eeeeee",
          },
      ],
  },
  {
      featureType: "poi",
      elementType: "labels.text",
      stylers: [
          {
              visibility: "off",
          },
      ],
  },
  {
      featureType: "all",
      elementType: "all",
      stylers: [
          {
              color: "#99FF33",
          },
      ],
  },
];

const Map: React.FC<propTypes> = () => {
  const [showCover, setShowCover] = useState(true)
  const [showTempCover, setShowTempCover] = useState(true)
  const [labelToShow, setLabelToShow] = useState<string>()
  const [screenShowing, setScreenShowing] = useState<string>()
  const [eventFeed, setEventFeed] = useState<any[]>([])
  const [currentLocation, setCurrentLocation] = useState<any>(null)

  const containerStyle = {
    width: '100%',
    height: '100%'
  };

  const labelMap = 
    (labelToShow === '1' || labelToShow === 'B1') ? 'Jasmin'
    : (labelToShow === '2' || labelToShow === 'B2') ? 'Becca'
    : (labelToShow === '3' || labelToShow === 'B3') ? 'Srija'
    : (labelToShow === '4' || labelToShow === 'B4') ? 'Rob x'
    : null

  useEffect(() => {
    if(showCover) {
      setScreenShowing('cover')
    } else {
      setScreenShowing(labelMap || 'error state')
    }
  }, [showCover, labelMap])
  
  const center = labelToShow === '2' ? {
    lat: 51.51336442702567,
    lng: -0.1115881978581923
  } : labelToShow === '3' ? {
    lat: 51.50863379794651,
    lng: -0.08578706303728129
  } : labelToShow === '1' ? {
    lat: 51.51384951539797,
    lng: -0.14177141864558415
  } : labelToShow === '4' ? {
    lat: 51.521509819679366, 
    lng: -0.08578448398363353
  } : {
    lat: 1,
    lng: 1
  };

  const { isLoaded } = useJsApiLoader({
    id: '4e5ba0e9b742c434',
    googleMapsApiKey: "AIzaSyAmOkeofVmICUGEchbLe0X2lbU-39n8ck8"
  })

  const [map, setMap] = React.useState(null)

  const onLoad = React.useCallback(function callback(map) {
    // This is just an example of getting and using the map instance!!! don't just blindly copy!
    // const bounds = new window.google.maps.LatLngBounds(center);
    // map.fitBounds(bounds);

    setMap(map)
  }, [])

  const onUnmount = React.useCallback(function callback(map) {
    setMap(null)
  }, [])

  const zoomValue = 15

  var q = faunadb.query
  var client = new faunadb.Client({
    secret: 'fnAFa3z0Q9ACUNUA--aVvkvtdmOOGhbO_8eyKwzG',
    domain: 'db.fauna.com',
    scheme: 'https',
  })

  const clickButton = () => {
    setShowCover(false)
    client.query(q.Update(q.Ref(q.Collection('map'), '390534782303339089'), {
      data: {
        controllerSetting: '1',
        showCoverOnClient: 'false'
      }
    }))

    setEventFeed(current => [...current, {
      text: 'Hidden cover screen and showing temp cover',
      datetime: Date.now()
    }])

    setTimeout(() => {
      setShowTempCover(false)
      setEventFeed(current => [...current, {
        text: 'Hidden all covers and showing marker 1',
        datetime: Date.now()
      }])
    }, 3000)
  }

  useEffect(() => {
    const getData = () => {
      client.query(q.Get(q.Match("mapIndex", 1)))
        .then((res: any) => {
          // setCurrentShowing(res.data.mapClientSeeing)
          setEventFeed([...res.data.eventFeed, {
            text: 'Map health check',
            datetime: Date.now()
          }])
          if(labelToShow !== res.data.controllerSetting)
            setLabelToShow(res.data.controllerSetting)
          
          setShowCover(res.data.showCoverOnClient === 'true')
          setShowTempCover(res.data.showCoverOnClient === 'true')
          // setLastCheckIn(res.data.mapClientLastRequest)
        })
        .catch(function (err) { 
          setEventFeed(current => [...current, {
            text: `error: ${JSON.stringify(err)}`,
            datetime: Date.now()
          }])
         })
    }

    getData()

    const interval = setInterval(() => {
      getData()
    }, 60000)

    return () => clearInterval(interval)
  }, []);

  useEffect(() => {
    client.query(q.Update(q.Ref(q.Collection('map'), '390534782303339089'), {
      data: {
        eventFeed
      }
    }))
  }, [eventFeed])

  useEffect(() => {
    if(screenShowing) {
      client.query(q.Update(q.Ref(q.Collection('map'), '390534782303339089'), {
        data: {
          mapClientSeeing: showCover ? 'cover' : screenShowing
        }
      }))
    }
  }, [screenShowing, showCover])

  useEffect(() => {
    if(!showCover && isLoaded) {
      setEventFeed(current => [...current, {
        text: `Showing label ${labelToShow}`,
        datetime: Date.now()
      }])
    } else if(showCover) {
      setEventFeed(current => [...current, {
        text: `Showing cover screen`,
        datetime: Date.now()
      }])
    }
  }, [labelToShow, showCover, isLoaded])

  // const calcCrow = (lat1, lon1, lat2, lon2) => {
  //   var R = 6371; // km
  //   var dLat = toRad(lat2-lat1);
  //   var dLon = toRad(lon2-lon1);
  //   var lat1 = toRad(lat1);
  //   var lat2 = toRad(lat2);

  //   var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
  //     Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
  //   var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  //   var d = R * c;
  //   return d;
  // }

  //   // Converts numeric degrees to radians
  // const toRad = (val: number) => {
  //   return val * Math.PI / 180;
  // }

  // const distanceToMarker = calcCrow()

  const Map = useMemo(() =>
  {
    return (
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={Boolean(labelToShow) ? center : undefined}
        // possibly defaultCenter
        zoom={zoomValue}
        onLoad={onLoad}
        onUnmount={onUnmount}
        options={{
          mapId: '4e5ba0e9b742c434',
          fullscreenControl: false,
          streetViewControl: false,
          zoomControl: false,
          mapTypeControl: false,
          // maxZoom: zoomValue,
          // minZoom: zoomValue
        }}
        // options={{
        //   styles: exampleMapStyles,
        // }}
      >
        { /* Child components, such as markers, info windows, etc. */ }
        {/* {currentLocation && <Marker position={{lat: currentLocation.lat, lng: currentLocation.long}} label="You" icon={{url: nameLabel, scaledSize: { width: 100, height: 100, equals: () => true }}} />} */}

        {/* boyf/girlf */}
        {labelToShow === '2' && <Marker position={{lat: 51.50890665906905, lng: -0.11708974625967929}} label="Becca" icon={{anchor: new google.maps.Point(50, 50), url: nameLabel, scaledSize: { width: 100, height: 100, equals: () => true }}} />}

        {/* first kiss */}
        {labelToShow === '3' && <Marker position={{lat: 51.50843866862295, lng: -0.08425157479056863}} label="Srija" icon={{anchor: new google.maps.Point(50, 50), url: nameLabel, scaledSize: { width: 100, height: 100, equals: () => true }}} />}

        {/* first date */}
        {labelToShow === '1' && <Marker position={{lat: 51.51384951539797, lng: -0.14177141864558415}} label="Jasmin" icon={{anchor: new google.maps.Point(50, 50), url: nameLabel, scaledSize: { width: 100, height: 100, equals: () => true }}} />}

        {labelToShow === '4' && <Marker position={{lat: 51.521509819679366, lng: -0.08578448398363353}} label="Rob" icon={{anchor: new google.maps.Point(50, 50), url: nameLabel, scaledSize: { width: 100, height: 100, equals: () => true }}} />}
      </GoogleMap>
    )

  }, [labelToShow])

  const backupImg = labelToShow === 'B1' ? jasmin
    : labelToShow === 'B2' ? becca
    : labelToShow === 'B3' ? srija
    : labelToShow === 'B4' ? rob
    : null

  return (<>
    {
      showCover ? (
        <MapCover clickButton={clickButton} />
      ) : (
        showTempCover ? <CoverImg src={cover} />
        : backupImg ? <>
          <BackupImage src={backupImg} />
          <MessageOverlay>Find {labelMap}</MessageOverlay>
        </>
        : isLoaded && (
          <>
            {Map}
            <MessageOverlay>Find {labelMap}</MessageOverlay>
          </>
        )
      )
    }
  </>);
}

export default Map;
