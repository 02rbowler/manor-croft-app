import React, { useContext } from 'react';
import styled from 'styled-components';
import Heading from '../components/Heading';
import Row, { ColouredRow } from '../components/Row';
import RowColumn from '../components/RowColumn';
import packageInfo from './../../package.json';
import {FaSpotify} from 'react-icons/fa'
import {MdAccountCircle} from 'react-icons/md'
import { AccountContext } from '../App';
import { AccountType } from '../types';

const RowIcon = styled.div`
  color: white;
  background-color: #484848;
  border: 1px solid #696363;
  margin-right: 10px;
  padding: 5px;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
  display: flex;
`

const Link = styled.a`
  color: white;
  padding: 5px;
  background-color: #5a5a5a;
  margin-right: 5px;
  text-align: center;
  display: block;
  border-radius: 5px;
  text-decoration: none;
`

const Select = styled.select`
  width: calc(100% - 5px);
  margin-right: 5px;
  border: 0;
  border-radius: 5px;
  padding: 5px;
`

const Login: React.FC = () => {
  const {account, updateAccount} = useContext(AccountContext);

  const updateAccountVal = (option: AccountType) => {
    localStorage.setItem('manor_croft_app_account', option || "");
    updateAccount(option);
  }

  return (
    <>
      <Row>
        <Heading>Settings</Heading>
      </Row>
      <ColouredRow>
        <RowIcon>
          <MdAccountCircle size={20} />
        </RowIcon>
        <RowColumn>Account</RowColumn>
        <RowColumn>
          <Select value={account as string} onChange={(e) => {updateAccountVal(e.target.value as AccountType)}}>
            <option value=""></option>

            {/* <option value="rob_bedroom">Rob Bedroom</option>
            <option value="manorcroft_kitchen">Manorcroft Kitchen</option>
            <option value="manorcroft_tech">Manorcroft Tech</option> */}
          </Select>
        </RowColumn>
      </ColouredRow>
      {account === "rob_bedroom" && <ColouredRow>
        <RowIcon>
          <FaSpotify size={20} />
        </RowIcon>
        <RowColumn>Spotify</RowColumn>
        <RowColumn>
          <Link href={`${process.env.REACT_APP_ENDPOINT}spotify/authorise`}>Connect account</Link>
        </RowColumn>
      </ColouredRow>}


      <Row>
        LoginVersion: {packageInfo.version}
      </Row>
    </>
    );
}

export default Login;
