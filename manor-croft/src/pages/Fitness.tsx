import React, { useContext, useState } from 'react';
import Heading from '../components/Heading';
import TextBox from '../components/TextBox';
import { Channel } from 'pusher-js';
import Row from '../components/Row';
import styled from 'styled-components';
import { AccountContext } from '../App';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { fetchExerciseData, updateExerciseData } from '../api';
import ShadowedButton from '../components/ShadowedButton';

const ProgressWrapper = styled.div`
  width: calc(100% - 40px);
  background-color: #211f3f;

  margin: 20px;
  margin-top: 0px;
  border-radius: 5px;
  overflow: hidden;
`

const ProgressBar = styled.div<{percentage: number}>`
  width: ${props => props.percentage}%;
  background-color: white;
  padding: 10px 0;

  span {
    font-weight: bold;
    margin-left: 10px;
    color: #0070af;
  }
`

const TextBoxWrapper = styled.div`
  display: flex;
  align-items: end;
  flex-wrap: wrap;

  div {
    flex: 1;
    margin-bottom: 5px;
  }

  h3 {
    margin: 0;
    padding: 0 5px;
  }
`

interface PropTypes {
  channel?: Channel
  accountNumber: number | null
}

const Fitness = ({channel, accountNumber}: PropTypes) => {
    const [startingWeight, setStartingWeight] = useState(localStorage.getItem('startingWeight') || '');
    const [targetWeight, setTargetWeight] = useState(localStorage.getItem('targetWeight') || '');
    const [newWeight, setNewWeight] = useState('');
    const {account} = useContext(AccountContext);
    const [message, setMessage] = useState('');

    const queryClient = useQueryClient()
 
    // Queries
    const query = useQuery('exercise', () => fetchExerciseData(account, channel, accountNumber))
    const currentDataSet = query?.data && query.data[0]?.data.data;
    const currentRef = query?.data && query.data[0]?.ref["@ref"].id
    console.log(currentRef)

    const addMutation = useMutation(updateExerciseData, {
      onSuccess: () => {
        // Invalidate and refetch
        setMessage('Successfully added!')
        queryClient.invalidateQueries('exercise')
      },
      onError: () => {
        setMessage('There was an error, Rob has messed up!')
      }
    })

    const handleSubmit = () => {
      console.log("handle submit")
      localStorage.setItem('startingWeight', startingWeight)
      localStorage.setItem('targetWeight', targetWeight)
      const startingInKg = (parseInt(startingWeight.split("|")[0] || "0") * 6.350293) + (parseInt(startingWeight.split("|")[1] || "0") * 0.453592)
      const targetInKg = (parseInt(targetWeight.split("|")[0] || "0") * 6.350293) + (parseInt(targetWeight.split("|")[1] || "0") * 0.453592)
      const newInKg = (parseInt(newWeight.split("|")[0] || "0") * 6.350293) + (parseInt(newWeight.split("|")[1] || "0") * 0.453592)
      const targetToLose = startingInKg - targetInKg
      const amountLost = startingInKg - newInKg
      console.log(targetToLose, amountLost)
      const percentageLost = Math.round(Math.min(Math.max((amountLost / targetToLose) * 100, 0), 100))
      console.log(percentageLost)
      addMutation.mutate({
        accountNumber,
        text: currentDataSet ? [...currentDataSet, percentageLost] : [percentageLost],
        ref: currentRef
      })
    }

    console.log(startingWeight === "" || targetWeight === "" || newWeight === "")
    const lastPercentageToShow = currentDataSet ? currentDataSet[currentDataSet.length - 1] : null
    return (
      <>
        <Row>
          <Heading>Fitness</Heading>
        </Row>
        <Row>
          <h3>Starting weight</h3>
          <TextBoxWrapper>
            <TextBox value={startingWeight.split("|")[0]} onChange={(inputText: string) => {
              setStartingWeight(`${inputText}|${startingWeight.includes("|") ? startingWeight.split("|")[1] : ""}`)
            }} type="number" />
            <h3>st.</h3>
            <TextBox value={startingWeight.split("|")[1]} onChange={(inputText: string) => {
              setStartingWeight(`${startingWeight.includes("|") ? startingWeight.split("|")[0] : ""}|${inputText}`)
            }} type="number" />
            <h3>lbs</h3>
          </TextBoxWrapper>
        </Row>
        <Row>
          <h3>Goal weight</h3>
          <TextBoxWrapper>
            <TextBox value={targetWeight.split("|")[0]} onChange={(inputText: string) => {
              setTargetWeight(`${inputText}|${targetWeight.includes("|") ? targetWeight.split("|")[1] : ""}`)
            }} type="number" />
            <h3>st.</h3>
            <TextBox value={targetWeight.split("|")[1]} onChange={(inputText: string) => {
              setTargetWeight(`${targetWeight.includes("|") ? targetWeight.split("|")[0] : ""}|${inputText}`)
            }} type="number" />
            <h3>lbs</h3>
          </TextBoxWrapper>
        </Row>
        <Row>
          <h3>New weight measurement</h3>
          <TextBoxWrapper>
            <TextBox value={""} onChange={(inputText: string) => {
              setNewWeight(`${inputText}|${newWeight.includes("|") ? newWeight.split("|")[1] : ""}`)
            }} type="number" />
            <h3>st.</h3>
            <TextBox value={""} onChange={(inputText: string) => {
              setNewWeight(`${newWeight.includes("|") ? newWeight.split("|")[0] : ""}|${inputText}`)
            }} type="number" />
            <h3>lbs</h3>
          </TextBoxWrapper>
        </Row>
        { message && <Row>
          <h4 style={{backgroundColor: '#211f3f', padding: 5}}>{message}</h4>  
        </Row>}
        <Row>
          <ShadowedButton 
            disabled={startingWeight === "" || targetWeight === "" || newWeight === ""}
            clickHandler={handleSubmit}
          >Submit</ShadowedButton>
        </Row>
        {lastPercentageToShow && <div style={{marginTop: 50}}>
          <Row><h3>Progress to goal</h3></Row>
          <ProgressWrapper>
            <ProgressBar percentage={lastPercentageToShow}>
              <span>{lastPercentageToShow}%</span>
            </ProgressBar>
          </ProgressWrapper>
        </div>}
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </>
    );
}

export default Fitness;
