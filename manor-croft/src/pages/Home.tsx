import React, { useContext, useEffect, useState } from 'react';
import BigButton from '../components/BigButton';
import Heading from '../components/Heading';
import SlidingRow from '../components/SlidingRow';

import { IoMdTv, IoMdTrain, IoMdToday, IoMdSunny, IoMdRefresh, IoIosNotifications } from "react-icons/io";
import { FaPodcast } from "react-icons/fa";
import Row from '../components/Row';
import { Channel } from 'pusher-js';
import styled from 'styled-components';
import { colours } from '../GlobalStyle';
import { pushCommandToDashboard, pushToDashboard } from '../api';
import DashboardData from '../modals/DashboardData';
import { getIcon } from '../services/Weather';
import ConfigureDashboard from '../modals/ConfigureDashboard';
import { AccountContext, ConfigContext } from '../App';
// import { useAuth0 } from '@auth0/auth0-react';

type propTypes = {
  channel?: Channel
}

const weatherApiUrl = 'https://api.openweathermap.org/data/2.5/';

const StyledButton = styled.button`
  padding: 5px 10px;
  border-radius: 5px;
  background: linear-gradient(24deg,#ff1f91,#9200ce);
  color: white;
  border: 1px solid #ff5dfa;
  box-shadow: 0px 0px 2px 0px #000742;
  font-weight: bold;
  font-size: 1em;
  display: flex;
  align-items: center;

  span {
    margin-left: 10px;
  }
`

const RightSection = styled.div`
  flex: 1;
  text-align: right;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  margin-bottom: 25px;
`

const Highlight = styled.div`
  background-color: ${colours.darkBlue};
  padding: 5px;
  border-radius: 5px;
  border: 1px solid #5f5f5f;
  width: fit-content;
  margin-left: auto;
  color: #c1c1c1;
  margin-top: 5px;
`

const WeatherText = styled.h2`
  margin: 0;
  font-weight: normal;
  display: flex;
  justify-content: flex-end;
`

interface Weather {
  temperature: string | number
  icon: React.ReactNode
}

const Home: React.FC<propTypes> = ({channel}) => {
  // const { user, isAuthenticated, isLoading } = useAuth0();

  const [greetingMessage, setGreetingMessage] = useState("Morning");
  const [weather, setWeather] = useState<Weather>({temperature: "-", icon: null});
  const {account} = useContext(AccountContext);
  const currentConfig = useContext(ConfigContext);
  const showButton = account === "rob_bedroom";

  useEffect(() => {
    const date = new Date();
    if(date.getHours() < 12) {
      setGreetingMessage("Morning");
    } else if(date.getHours() < 18) {
      setGreetingMessage("Afternoon");
    } else {
      setGreetingMessage("Evening");
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setGreetingMessage]);

  const location = "london";
  // TO CHANGE

  useEffect(() => {
    fetch(weatherApiUrl + 'weather?q=' + location + '&units=metric&APPID=' + process.env.REACT_APP_WEATHER_ID).then(res => {
      return res.json()
    }).then(res => {
      const temp = parseInt(res.main.temp, 10);
      const weather = res.weather;
      setWeather({temperature: temp, icon: getIcon(weather[0].main) });
    }).catch(err => {
      // Sentry.captureException("Weather fetch failed (reducer): " + err);
    });

  }, [])

  return (<>
    <Row flex>
      <Heading>
        Good<br />
        {greetingMessage}
      </Heading>
      <RightSection>
        <WeatherText>
          {weather.icon}&nbsp;{weather.temperature}&deg;
        </WeatherText>
        <Highlight>{currentConfig?.data.displayName}</Highlight>
      </RightSection>
    </Row>
      
    <SlidingRow>
      <BigButton text="TV" icon={<IoMdTv size={25} />} clickHandler={() => pushToDashboard('tv', account, channel)} />
      {showButton && <BigButton text="TFL" icon={<IoMdTrain size={25} />} clickHandler={() => pushToDashboard('tube', account, channel)} />}
      {showButton && <BigButton text="Podcasts" icon={<FaPodcast size={25} />} clickHandler={() => pushToDashboard('podcast', account, channel)} />}
      <BigButton text="News" icon={<IoMdToday size={25} />} clickHandler={() => pushToDashboard('news', account, channel)} />
      <BigButton text="Weather" icon={<IoMdSunny size={25} />} clickHandler={() => pushToDashboard('weather', account, channel)} />
      <BigButton text="Reminders" icon={<IoIosNotifications size={25} />} clickHandler={() => pushToDashboard('reminders', account, channel)} />
    </SlidingRow>
    <Row>
      <StyledButton onClick={() => pushCommandToDashboard('refresh', account, channel)}>
        <IoMdRefresh size={25} />
        <span>Refresh</span>
      </StyledButton>
    </Row>
    <Row>
      <ConfigureDashboard account={account} />
    </Row>
    <Row>
      <DashboardData />
    </Row>
  </>);
}

export default Home;
