import React from 'react';
import styled from 'styled-components';
import { breakpoints } from '../GlobalStyle';

const ButtonElem = styled.button`
  display: inline-block;
  padding: 5px 10px;
  border-radius: 10px;
  background: linear-gradient(24deg,#091adc,#35f6ff);
  color: white;
  border: 1px solid #08afff;
  margin: auto 5px;
  height: 110px;
  width: calc(100% / 3 - 10px);
  overflow: hidden;
  box-shadow: 0px 0px 2px 0px #000742;
  font-weight: bold;
  font-size: 1.2em;
  margin-bottom: 10px;
  text-align: left;

  @media only screen and (max-width: 340px) {
    width: calc(100% / 2 - 10px);
  }

  ${breakpoints.widescreen} {
    width: 120px;
  }

  &:hover {
    background-color: #e6e6e6;
    cursor: pointer;
  }
`;

const IconCircle = styled.div`
  border-radius: 5px;
  background-color: #242244;
  border: 1px solid #a2a2a2;
  width: 28px;
  padding: 4px;
  height: 28px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 5px;
`;

interface buttonProps {
  class?: string,
  clickHandler: Function,
  text: string,
  icon?: React.ReactNode;
}

const BigButton = (props: buttonProps) => {
  return (
    <ButtonElem className={props.class} onClick={(e) => { props.clickHandler() }}>
      <IconCircle>{props.icon}</IconCircle>
      {props.text}
    </ButtonElem>
  );
}

export default BigButton;
