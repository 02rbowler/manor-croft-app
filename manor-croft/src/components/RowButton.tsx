import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
    width: 100%;
    font-weight: bold;
    padding: 0 15px;
    border-radius: 10px;
    text-align: left;
    background: linear-gradient(24deg,#ff1295,#5605f3);
    color: white;
    border: 1px solid #f55bff;
    display: block;

    &:hover {
        background-color: #cba800;
        color: white;
        cursor: pointer;
    }
`;

interface props {
  clickHandler?: Function,
  text: string
}

const RowButton: React.FC<props> = ({clickHandler, text}) => (
    <Button onClick={() => clickHandler && clickHandler()}>
        <h3>{text}</h3>
    </Button>
)

export default RowButton;
