import React from 'react';
import styled from 'styled-components';
import { colours } from '../GlobalStyle';

const RowElem = styled.div<{flex?: boolean}>`
    padding: 10px 20px;
    text-align: left;
    ${props => props.flex ? 'display: flex;' : ''}
`;

function Row(props: { children: React.ReactNode; flex?: boolean }) {
    return (
        <RowElem flex={!!props.flex}>
            { props.children }
        </RowElem>
    );
}

export const ColouredRow = styled.div`
  background-color: ${colours.darkBlue};
  border: 1px solid #7872b7;
  display: flex;
  padding: 5px;
  border-radius: 10px;
  margin: 10px 20px;
  margin-top: 0;
  display: flex;
  align-items: center;
`

export default Row;
