import React, { useContext, useState } from 'react';
import styled from 'styled-components';
import { IoMdCalendar, IoIosSettings, IoIosTimer, IoMdList, IoMdCamera } from "react-icons/io";
import { IoBarbellOutline } from "react-icons/io5";
import NavOption from './NavOption';
import { breakpoints, colours } from '../GlobalStyle';
import { AccountContext } from '../App';
import { useLocation } from 'react-router-dom';

const isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !(window as any).MSStream;

const NavElem = styled.div`
  width: max-content;
  display: table;
  margin-left: auto;
  margin-right: auto;
`;

const NavWrapper = styled.nav`
  overflow-x: auto;
  width: 100%;
  position: fixed;
  bottom: 0;
  background-color: ${colours.darkBlue};
  ${isIOS && 'padding-bottom: 20px;'}

  ${breakpoints.widescreen} {
    position: relative;
    width: auto;
    padding-top: 30px;
  }
`;

function Nav() {
  const {account} = useContext(AccountContext);
  const location = useLocation();

  // Get current path
  const pathname = location.pathname;
  const [selected, setSelected] = useState(pathname);

  if(pathname === '/map') {
    return null
  }

  return (
    <NavWrapper>
      <NavElem>
          <NavOption selected={selected === '/reminders'} clickHandler={(text: string) => setSelected(text)} 
            icon={<IoIosTimer size={32} />} text="Reminders" link="/reminders" />
          <NavOption selected={selected === '/'} clickHandler={(text: string) => setSelected(text)} 
            icon={<IoMdCalendar size={32} />} text="Dashboard" link="/" />
          {account === "rob_bedroom" && <>
            <NavOption selected={selected === '/watchlist'} clickHandler={(text: string) => setSelected(text)} 
              icon={<IoMdList size={32} />} text="Watchlist" link="/watchlist" />
            <NavOption selected={selected === '/fitness'} clickHandler={(text: string) => setSelected(text)} 
              icon={<IoBarbellOutline size={32} />} text="Fitness" link="/fitness" />
          </>}
          {account === "manorcroft_kitchen" && <NavOption selected={selected === '/CCTV'} clickHandler={(text: string) => setSelected(text)} 
            icon={<IoMdCamera size={32} />} text="CCTV" link="/cctv" />}
          <NavOption selected={selected === '/settings'} clickHandler={(text: string) => setSelected(text)} 
            icon={<IoIosSettings size={32} />} text="Settings" link="/settings" />
      </NavElem>
    </NavWrapper>
  );
}

export default Nav;
