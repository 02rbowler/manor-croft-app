import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { VscChromeClose } from 'react-icons/vsc';
import { breakpoints, colours } from "../GlobalStyle";

const modalRoot = document.getElementById('modal-root');

const Overlay = styled.div`
    ${breakpoints.widescreen} {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        background-color: #000000c9;
    }
`

const ModalContainer = styled.section`
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    // background-color: white;
    background-color: ${colours.darkBlue};
    color: white;
    overflow-y: auto;

    ${breakpoints.widescreen} {
        width: 600px;
        margin: auto;
        height: 80%;
        border: 1px solid #a98dff;
        border-radius: 20px;
    }
`

const CloseButton = styled.button`
    font-weight: bold;
    color: #b1b1b1;
    border: 0;
    background: none;
    padding: 10px;
    position: absolute;
    top: 0;
    right: 0;
`

const ModalTitle = styled.h3`
    color: #b1b1b1;
    font-size: 15px;
    margin-left: 10px;
`

const FixedHeader = styled.div`
    position: sticky;
    top: 0;
`

const ModalElement: React.FC<{title?: string, closeModalFunc: Function}> = ({ title, closeModalFunc, children }) => {
    return <Overlay>
        <ModalContainer>
            <FixedHeader>
                <ModalTitle>{title}</ModalTitle>
                <CloseButton onClick={() => closeModalFunc()}><VscChromeClose size={30} /></CloseButton>
            </FixedHeader>
            {children}
        </ModalContainer>
    </Overlay>
}

const Modal: React.FC<{title?: string, closeModalFunc: Function}> = ({title, closeModalFunc, children}) => {
    const comp = document.createElement('div');

    useEffect(() => {
        modalRoot?.appendChild(comp);

        return () => {
            modalRoot?.removeChild(comp);
        }
    }, [comp])

    return ReactDOM.createPortal(
        <ModalElement title={title} closeModalFunc={closeModalFunc}>{children}</ModalElement>,
        comp
    )
}

export default Modal;