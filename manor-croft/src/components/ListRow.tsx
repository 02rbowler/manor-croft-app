import React from 'react';
import styled from 'styled-components';

const StyledListRow = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    padding: 7px 0;
    box-sizing: border-box;
    border-bottom: 1px solid #baa0f5;

    &.headerRow {
        border-bottom: 0;
        font-weight: bold;
    }

    &:last-of-type {
        border-bottom: 0;
    }

    span:first-of-type {
        flex: 1 1 auto;
    }
`;

function ListRow(props: any) {

  return (
    <StyledListRow className={props.headerRow && 'headerRow'}>
        {props.children}
    </StyledListRow>
  );
}

export default ListRow;
