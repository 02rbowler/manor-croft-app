import React from 'react';
import styled from 'styled-components';

const HeadingElem = styled.h1`
    text-align: left;
    display: inline-block;
    font-size: 40px;
    line-height: 40px;
`;

function Heading(props: { children: React.ReactNode; }) {
  return (
    <HeadingElem>
        { props.children }
    </HeadingElem>
  );
}

export default Heading;
