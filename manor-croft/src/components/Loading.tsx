import React from 'react';
import styled from 'styled-components';
import {AiOutlineLoading3Quarters} from "react-icons/ai";

const LoadingWrapper = styled.div`
  height: 100vh;
  justify-content: center;
  display: flex;
  flex-direction: column;
  align-items: center;

  div {
    animation: spin 1s linear infinite;
    display: flex;
  }

  @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }
`;

function Loading(props: any) {

  return (
    <LoadingWrapper>
      <div>
        <AiOutlineLoading3Quarters size={50} />
      </div>
    </LoadingWrapper>
  );
}

export default Loading;
