import React from 'react';
import styled from 'styled-components';
import {
    Link
  } from "react-router-dom";
import { breakpoints } from '../GlobalStyle';

const NavOptionElem = styled.span`
    a {
        text-align: center;
        display: table-cell;
        padding: 20px 15px;
        padding-top: 10px;
        min-width: 80px;
        text-decoration: none;
        color: black;

        ${breakpoints.widescreen} {
            display: block;
        }
    }

    @media only screen and (max-width: 376px) {
        a {
            padding: 15px 15px;
            padding-top: 10px;
            min-width: 40px;
        }
    }
`;

const OptionText = styled.div`
    font-size: 0.8em;
    color: white;

    &.selected {
        color: #e4bd00;
    }
`;

const OptionIcon = styled.div`
    color: white;

    &.selected {
        color: #e4bd00;
    }
`;

interface propTypes {
    icon: React.ReactNode;
    text: string;
    link: string;
    selected: boolean;
    clickHandler: Function;
}

function NavOption(props: propTypes) {
    const selectedClass = props.selected ? 'selected' : '';
    
    return (
        <NavOptionElem onClick={() => {props.clickHandler(props.link)}}>
            <Link to={props.link}>
                <OptionIcon className={selectedClass}>{props.icon}</OptionIcon>
                <OptionText className={selectedClass}>{props.text}</OptionText>
            </Link>
        </NavOptionElem>
    );
}

export default NavOption;
