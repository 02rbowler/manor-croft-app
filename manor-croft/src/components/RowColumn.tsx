import React from 'react';
import styled from 'styled-components';

const ColumnElem = styled.div`
    flex: 1 1 auto;
`;

function RowColumn(props: { children: React.ReactNode; }) {
    return (
        <ColumnElem>
            { props.children }
        </ColumnElem>
    );
}

export default RowColumn;
