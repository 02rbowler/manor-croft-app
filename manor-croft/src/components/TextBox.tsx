import React, {useState} from 'react';
import styled from 'styled-components';

const TextElem = styled.input`
    margin-right: 10px;
    border: none;
    border-radius: 10px;
    font-size: 20px;
    flex: 1 1 auto;

    &:focus {
        outline: none;
    }
`;

const StyledTextBox = styled.div`
  background-color: white;
  border-radius: 10px;
  padding: 10px;
  display: flex;
  flex-direction: row;
`

const Button = styled.span`
    font-weight: bold;
    cursor: pointer;
    display: flex;
    flex-direction: column;
    justify-content: center;
    color: white;
    background: linear-gradient(24deg,#ff1f91,#9200ce);
    margin: -5px;
    border-radius: 6px;
    padding: 0 10px;
    border: 1px solid #ff5dfa;
`;

const TextButton = (props: { children: React.ReactNode; clickHandler: Function; }) => {
  return (
    <Button onClick={(e) => { props.clickHandler() }}>
        { props.children }
    </Button>
  );
}

interface props {
  value: string
  placeholder?: string
  clickHandler?: Function
  onChange?: Function
  withButton?: string
  type?: "text" | "number"
}

const TextBox: React.FC<props> = ({ value, placeholder, clickHandler, onChange, withButton, type }) => {
  const [inputText, setInputText] = useState(value);

  const handleKeyDown = (event: { key: string; }) => {
    if (event.key === 'Enter' && withButton) {
      handleClick();
    }
  }

  const handleClick = () => {
    clickHandler && clickHandler(inputText);
    setInputText("");
  }

  return (
    <StyledTextBox>
      <TextElem type={type || "text"} value={inputText} placeholder={placeholder} 
        onChange={(e) => { 
          setInputText(e.target.value); 
          onChange && onChange(e.target.value);
        }} onKeyDown={handleKeyDown}
        inputMode={type === "number" ? "decimal" : "text"}
      >
      </TextElem>
      { withButton && 
        <TextButton clickHandler={handleClick}>Done</TextButton>
      }
    </StyledTextBox>
  );
}

export default TextBox;
