import React from 'react';
import styled from 'styled-components';

const TableWrapper = styled.div`
  border: 1px solid #5448da;
  margin: 0 20px;
  margin-bottom: 20px;
  border-radius: 10px;
  overflow: hidden;
`

const StyledTable = styled.table`
  width: 100%;
  border-collapse: collapse;

  th {
    text-align: left;
    padding: 15px;
  }

  tr {
    background-color: #383271;
    border-bottom: 1px solid #8a8a8a;

    &:last-of-type {
      border: 0;
    }

    &.nobackground {
      background-color: transparent;
    }
    
    td {
      padding: 10px;
    }
  }
`

export const TableButton = styled.button`
  color: white;
  background-color: #5d40d0;
  border: 1px solid #b95cff;
  margin-left: 10px;
  padding: 5px;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
  display: inline-flex;
  font-size: 1em;
`

export const TableOutlineButton = styled(TableButton)`
  background-color: transparent;
  border: none;
`

const Table: React.FC = ({children}) => {
  return <TableWrapper>
    <StyledTable>{children}</StyledTable>
  </TableWrapper>
}

export default Table;
