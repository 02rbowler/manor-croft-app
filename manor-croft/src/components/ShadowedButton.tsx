import React from 'react';
import styled from 'styled-components';
import { colours } from '../GlobalStyle';

const ButtonElem = styled.button`
  box-shadow: 0 0 16px 2px #0079ab;
  background-color: ${colours.darkBlue};
  border: 1px solid #7872b7;
  color: white;
  width: 100%;
  padding: 10px;
  font-size: 20px;
  border-radius: 10px;

  &:hover {
      background-color: #e6e6e6;
      cursor: pointer;
  }
`;

// const Blur = styled.div`
//   background: conic-gradient(red, orange, yellow, blue);
//   padding: 5px;
//   filter: blur(10px);
// `;

// interface buttonProps {
//   class?: string,
//   clickHandler: Function,
//   text: string,
//   icon?: React.ReactNode;
// }

const ShadowedButton: React.FC<{clickHandler: Function, disabled?: boolean}> = ({clickHandler, disabled, children}) => {
  return (
    <ButtonElem onClick={(e) => clickHandler()} disabled={disabled}>
      {children}
    </ButtonElem>
  );
}

export default ShadowedButton;
