import React from 'react';
import styled from 'styled-components';

const RowElem = styled.div`
    border-top: 1px solid #d8d8d8;
    border-bottom: 1px solid #d8d8d8;
    text-align: left;
    margin-bottom: 20px;
`;

function RowContainer(props: { children: React.ReactNode; }) {
    return (
        <RowElem>
            { props.children }
        </RowElem>
    );
}

export default RowContainer;
