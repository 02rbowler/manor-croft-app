import React from 'react';
import styled from 'styled-components';

const ButtonElem = styled.button`
  display: inline-block;
  padding: 5px 15px;
  border-radius: 5px;
  background-color: #f1c700;
  border: 1px solid black;

  &:hover {
    background-color: #cba800;
    color: white;
    cursor: pointer;
  }

  &.red {
    background-color: red;
    color: white;
    border-color: #c70303;

    &:hover {
      background-color: #c70303;
    }
  }
`;

interface buttonProps {
  class?: string,
  clickHandler: Function,
  text: string
}

function Button(props: buttonProps) {
  return (
    <ButtonElem className={props.class} onClick={(e) => { props.clickHandler() }}>
        {props.text}
    </ButtonElem>
  );
}

export default Button;
