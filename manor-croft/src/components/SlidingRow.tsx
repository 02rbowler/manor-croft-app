import React from 'react';
import styled from 'styled-components';

const RowElem = styled.div`
    padding: 5px 15px;
    text-align: left;
    // overflow-x: auto;
    // white-space: nowrap;
`;

function SlidingRow(props: { children: React.ReactNode; }) {
    return (
        <RowElem>
            { props.children }
        </RowElem>
    );
}

export default SlidingRow;
