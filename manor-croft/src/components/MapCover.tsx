import React, { useEffect } from "react"
import styled from "styled-components"
import plainCover from '../assets/plainCover.jpeg';
import SpeechRecognition, { useSpeechRecognition } from "react-speech-recognition";
import Vara from "vara";

const CoverImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`

const CoverBack = styled.div`
  width: 100%;
  height: 100%;
`

const VaraText = styled.div`
  position: absolute;
  top: 50px;
  left: 40px;
  width: calc(100% - 80px);
`

export const MapCover = ({clickButton}: {clickButton: () => void}) => {
  const {
    transcript,
    listening,
    resetTranscript,
  } = useSpeechRecognition();

  useEffect(() => {
    SpeechRecognition.startListening({continuous: true})
    let transcriptInterval = setInterval(() => {
      resetTranscript()
    }, 120000)

    return () => {
      SpeechRecognition.stopListening()
      clearInterval(transcriptInterval)
    }
  }, []);

  if(transcript.includes('up to no good')) {
    clickButton()
  }

  useEffect(() => {
    var vara = new Vara(
      "#vara-container",
      "https://raw.githubusercontent.com/akzhy/Vara/master/fonts/Satisfy/SatisfySL.json",
      [
        {
          text: 'MESSRS. MOONY, WORMTAIL, PADFOOT & PRONGS ASK YOU TO SAY THE SPELL',
          fontSize: 40,
          strokeWidth: 0.7,
        },
      ]
    );
  }, []);

  console.log(transcript, listening)

  return (
    <CoverBack>
      <CoverImg src={plainCover} />
      <VaraText id="vara-container" />
    </CoverBack>
  )
}