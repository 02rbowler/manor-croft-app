import React, {Suspense, lazy, useState, useEffect} from 'react';
import OneSignal from 'react-onesignal'
import {
  QueryClient,
  QueryClientProvider,
} from 'react-query'
// import logo from './logo.png';
import {
  BrowserRouter as Router,
  Route,
  Routes,
  useLocation
} from "react-router-dom";
import Nav from './components/Nav';
import Loading from './components/Loading';
import withClearCache from "./ClearCache";
import styled from "styled-components";
import background from "./assets/background.jpg"
import { breakpoints } from './GlobalStyle';
import Pusher from 'pusher-js';
import { AccountType, Config } from './types';
import { Auth0Provider } from '@auth0/auth0-react';
import Login from './pages/Login';
import Watchlist from './pages/Watchlist';
import { getDashboardConfig } from './api';
import Fitness from './pages/Fitness';
import Map from './pages/Map';

const queryClient = new QueryClient()

const CCTV = lazy(() => import('./pages/CCTV'));
const Home = lazy(() => import('./pages/Home'));
const Settings = lazy(() => import('./pages/Settings'));
const StravaAuth = lazy(() => import('./pages/StravaAuth'));
const Reminders = lazy(() => import('./pages/Reminders'));

const ContentWrapperBackground = styled.div`
  background: url(${background}) no-repeat center center fixed;
  background-size: cover;
  min-height: 100vh;
`

const ContentWrapper = styled.div`
  background: linear-gradient(45deg,#231a3dfa,#26009c80);
  color: white;
  min-height: 100vh;
  display: flex;

  ${breakpoints.widescreen} {
    flex-direction: row-reverse;
  }
`

export const ConfigContext = React.createContext<Config | null>(null);
export const AccountContext = React.createContext<{account: AccountType, updateAccount: Function}>({
  account: null,
  updateAccount: () => {}
});

const MainApp = () => {
  // const storedAccount = localStorage.getItem('manor_croft_app_account');
  const [accountOption, setAccountOption] = useState(localStorage.getItem('manor_croft_app_account') as AccountType);
  const [configs, setConfigs] = useState<Config[]>([]);
  const [currentConfig, setCurrentConfig] = useState((configs.filter((item) => item.data.location === accountOption))[0]);
  const [pusher] = useState(new Pusher(process.env.REACT_APP_PUSHER_KEY || "", {
    cluster: process.env.REACT_APP_PUSHER_CLUSTER,
    authEndpoint: process.env.REACT_APP_NETLIFY_ENDPOINT +
        ".netlify/functions/pusherAuth",
  }));
  const [channel] = useState(pusher.subscribe('private-dashboard'));

  useEffect(() => {
    setCurrentConfig((configs.filter((item) => item.data.location === accountOption))[0]);
  }, [configs, accountOption]);

  useEffect(() => {
    const getConfig = async () => {
      const config = await getDashboardConfig()
      setConfigs(config)
    }

    getConfig()

    return () => {
      pusher.unsubscribe('private-dashboard');
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  
  useEffect(() => {
      OneSignal.init({
        appId: "b6184f6b-49d6-494e-9e11-2d46df0e63bc",
        allowLocalhostAsSecureOrigin: true,
        serviceWorkerPath: "push/OneSignalSDKWorker.js",
      })
  }, [])

  useEffect(() => {
    if(accountOption) {
      OneSignal.setEmail(accountOption)
    }
  }, [accountOption])

  const getAccountNumber = (accountOption: AccountType) => {
    if(!accountOption) {
      return null;
    }

    for(var i=0; i<configs.length; i++) {
      if(configs[i].data.location === accountOption) {
        return configs[i].data.userId
      }
    }

    return null;
  }

  return (
    <Auth0Provider
      domain="dev-093yrzg.eu.auth0.com"
      clientId="y2lxAz0bH0f7LZV2RWOL2Ma9Q6tB12sY"
      redirectUri={window.location.origin}
    >
      <QueryClientProvider client={queryClient}>
        <Router>
          <ContentWrapperBackground>
            <AccountContext.Provider value={{
              account: accountOption,
              updateAccount: (newVal: AccountType) => setAccountOption(newVal)
            }}>
              <ContentWrapper>
                <div style={{flex: 1, width: "100%", maxHeight: "100vh", overflowY: "auto"}}>
                  <ConfigContext.Provider value={currentConfig}>
                    <Suspense fallback={<Loading />}>
                      <Routes>
                        <Route path="/reminders" element={
                          <Reminders channel={channel} accountNumber={getAccountNumber(accountOption)}/>
                        } />
                        <Route path="/settings" element={
                          <Settings 
                            locations={configs.map(config => ({location: config.data.location, displayName: config.data.displayName}))}
                          />
                        } />
                        <Route path="/stravaAuth" element={
                          <StravaAuth />
                        } />
                        <Route path="/cctv" element={
                          <CCTV />
                        } />
                        <Route path="/watchlist" element={
                          <Watchlist channel={channel} accountNumber={getAccountNumber(accountOption)}/>
                        } />
                        <Route path="/fitness" element={
                          <Fitness channel={channel} accountNumber={getAccountNumber(accountOption)} />
                        } />
                        <Route path="/login" element={
                          <Login />
                        } />
                        <Route path="/map" element={
                          <Map />
                        } />
                        <Route path="/" element={
                          accountOption === 'test' ? <Map /> : <Home channel={channel} />
                        } />
                      </Routes>
                    </Suspense>
                  </ConfigContext.Provider>
                </div>
                {accountOption !== 'test' && <Nav />}
              </ContentWrapper>
            </AccountContext.Provider>
          </ContentWrapperBackground>
        </Router>
      </QueryClientProvider>
    </Auth0Provider>
  );
}

const ClearCacheComponent = withClearCache(MainApp);

const App = () => <ClearCacheComponent />

export default App;
